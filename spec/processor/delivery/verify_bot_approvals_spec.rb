# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/delivery/verify_bot_approvals'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::VerifyBotApprovals do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        from_security_fork?: true,
        target_branch_is_main_or_master?: true,
        gitlab_bot_event_actor?: true
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["merge_request.approval", "merge_request.approved"]

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event actor is not gitlab-bot' do
      before do
        allow(event).to receive(:gitlab_bot_event_actor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'out of policy' do
      it 'removes the approval' do
        body = add_automation_suffix('processor/delivery/verify_bot_approvals.rb') do
          <<~MARKDOWN.chomp
            :warning: This bot has been utilized to approve merge_requests outside
            of the allowed policies :warning:

            /unapprove
          MARKDOWN
        end

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end
    end

    it 'merge the merge request' do
      expect(event).to receive(:source_branch_is?).with('master').and_return(true)
      expect(subject).to receive(:merge_merge_request)

      subject.process
    end

    context 'when source project is not a security fork' do
      before do
        allow(event).to receive(:from_security_fork?).and_return(false)
      end

      include_examples 'out of policy'
    end

    context 'when event source branch is not master' do
      before do
        allow(event).to receive(:source_branch_is?).with('master').and_return(false)
      end

      include_examples 'out of policy'
    end

    context 'when event target branch is not master' do
      before do
        allow(event).to receive(:source_branch_is?).with('master').and_return(true)
        allow(event).to receive(:target_branch_is_main_or_master?).and_return(false)
      end

      include_examples 'out of policy'
    end
  end
end
