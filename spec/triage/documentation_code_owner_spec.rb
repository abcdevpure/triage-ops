# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/documentation_code_owner'

describe Triage::DocumentationCodeOwner do
  let(:project_id) { 123 }
  let(:merge_request_iid) { 888 }
  let(:api_response) { [] }

  subject { described_class.new(project_id, merge_request_iid) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules",
      response_body: api_response)
  end

  describe '#most_specific_approvers' do
    it 'returns a collection that allows selecting a random element' do
      expect(subject.most_specific_approvers).to respond_to(:sample)
    end

    context 'with a "Documentation Page" match' do
      let(:api_response) do
        [
          {
            rule_type: 'code_owner',
            name: '/doc/',
            section: 'Documentation Owners',
            eligible_approvers: [{ username: 'docs-owner' }]
          },
          {
            rule_type: 'code_owner',
            name: '/doc/**/*.md',
            section: 'Documentation Directories',
            eligible_approvers: [{ username: 'directory-owner' }]
          },
          {
            rule_type: 'code_owner',
            name: '/doc/ci/docker/using_docker_images.md',
            section: 'Documentation Page',
            eligible_approvers: [{ username: 'page-owner1' }, { username: 'page-owner2' }]
          }
        ]
      end

      it 'provides the most specific approvers' do
        expect(subject.most_specific_approvers).to contain_exactly('page-owner1', 'page-owner2')
      end
    end

    context 'when the approval rules contain no documentation sections' do
      let(:api_response) do
        [
          {
            rule_type: 'code_owner',
            eligible_approvers: [{ username: 'some-user' }],
            section: 'Engineering Productivity'
          }
        ]
      end

      it 'returns no technical writers' do
        expect(subject.most_specific_approvers).to be_empty
      end
    end

    context "when the approval rules don't contain code owners" do
      let(:api_response) do
        [
          {
            rule_type: 'non_documentation_rule_type',
            eligible_approvers: [{ username: 'some-user' }],
            section: 'Docs Create'
          }
        ]
      end

      it 'returns no approvers' do
        expect(subject.most_specific_approvers).to be_empty
      end
    end

    context 'when the API response contains no sections' do
      let(:api_response) do
        [
          {
            rule_type: 'code_owner',
            eligible_approvers: [],
            section: nil
          }
        ]
      end

      it 'returns no approvers' do
        expect(subject.most_specific_approvers).to be_empty
      end
    end

    context 'when the API response is missing approvers' do
      let(:api_response) do
        [
          {
            rule_type: 'code_owner',
            name: '/doc/',
            eligible_approvers: nil,
            section: 'Docs Create'
          }
        ]
      end

      it 'returns no approvers' do
        expect(subject.most_specific_approvers).to be_empty
      end
    end
  end
end
