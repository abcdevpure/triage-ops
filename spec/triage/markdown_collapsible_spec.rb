# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/markdown_collapsible'

RSpec.describe Triage::MarkdownCollapsible do
  let(:title) { 'Click me to expand!' }
  let(:body) { 'Merge request reminder message' }

  subject { described_class.new(title, content) }

  describe '#collapsed_content' do
    let(:content) { 'Merge request reminder message' }

    it 'returns a collapsible content' do
      expected_content = <<~SUMMARY
        <details>
        <summary markdown="span">#{title}</summary>

        #{body}
        </details>
      SUMMARY

      expect(subject.collapsed_content).to eq(expected_content)
    end
  end

  describe '#uncollapsed_content' do
    let(:content) do
      <<~SUMMARY
        <details>
        <summary markdown="span">#{title}</summary>

        #{body}
        </details>
      SUMMARY
    end

    it 'returns a uncollapsed content' do
      expect(subject.uncollapsed_content).to eq(body)
    end
  end
end
