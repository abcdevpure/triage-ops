# frozen_string_literal: true

require_relative '../triage/triage/event'

module CommunityContributionHelper
  def bot_author?
    automation_author? || author_is_gitlab_service_account?
  end

  private

  def automation_author?
    ::Triage::Event::AUTOMATION_IDS.include?(resource.dig(:author, :id))
  end

  def author_is_gitlab_service_account?
    author.match?(::Triage::Event::GITLAB_SERVICE_ACCOUNT_REGEX)
  end
end
