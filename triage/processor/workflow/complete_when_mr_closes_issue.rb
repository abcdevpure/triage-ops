# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  module Workflow
    class CompleteWhenMrClosesIssue < Processor
      react_to 'merge_request.merge'

      def applicable?
        event.from_gitlab_org? && workflow_automation_group
      end

      def process
        Triage.api_client.merge_request_closes_issues(project_id, event.iid).each do |issue|
          body = "/label ~\"#{Labels::WORKFLOW_AUTOMATION[workflow_automation_group]}\""
          noteable_path = "/projects/#{project_id}/issues/#{issue.iid}"

          add_comment(body, noteable_path, append_source_link: false)
        end
      end

      private

      def workflow_automation_group
        @workflow_automation_group ||= event.label_names.find do |label|
          Labels::WORKFLOW_AUTOMATION.key?(label)
        end
      end

      def project_id
        event.project_id
      end
    end
  end
end
